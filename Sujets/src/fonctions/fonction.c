#include <stdio.h>

// declaration d'une fonction:
int somme(int a, int b, int c) // entête
{
  a = a + b + c;
  return a;
}

int main(){
  int a=1, b=2, c=3;
  printf("valeurs initiale:\n");
  printf("a=%d b=%d c=%d\n", a, b, c);
  printf("\n");
  
  int r = somme(a,b,c);

  printf("somme=%d\n", r);
  printf("valeurs finales :\n");
  printf("a=%d b=%d c=%d\n", a, b, c);

  return 0;
}
