#include <stdio.h>

int main() {

  int a = 4;

  int* p; // création d'un pointeur nul:
          // (p) ne pointe sur rien car il n'est pas initialisé

  p = &a; // on fait pointer (p) sur l'adresse de (a)

  // quelques tests:
  printf("valeur de a: %d\n", a);
  printf("adresse mémoire de a: %p\n", (void *)&a); // affichage de l'adresse en hexadécimal
  printf("\n");

  printf("valeur du pointeur p: %p\n", (void *)p); // affichage de l'adresse sauvegardée dans (p) en hexadécimal
  printf("valeur de a en passant par le pointeur p: %d\n", *p);

  
  *p = 0; // modification de a en utilisant le pointeur
  printf("valeur de a: %d\n", a);

  return 0;
}
