#include <stdio.h>

int main() {
  int T[5] = {1, 2, 3, 4, 5};

  int* p = &T[0];

  for (int i = 0; i < 5; i++) {
    printf("&T[%i] = %p\n", i, (void *)&T[i]); // adresse de la case (i)
    printf("p = %p\n", (void *)p); // adresse pointée
    printf("*p = %d\n", *p); // valeur pointée
    printf("-----------------\n");

    p++; // le pointeur bouge sur la case suivante
  }
  return 0;
}
