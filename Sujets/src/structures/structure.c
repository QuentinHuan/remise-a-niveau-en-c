#include <stdio.h>

// definition d'une structure représentant un vecteur
struct Vector3{
    float x;
    float y;
    float z;
};

int main(){
    struct Vector3 v = {1.f, 2.f, 3.f}; // initialisation

    printf("v = {%f, %f, %f}", v.x, v.y, v.z); // on accède aux champs avec "."

    struct Vector3* p = &v;
    p->x = 3.f; // si on passe par un pointeur, on remplace "." par "->"

    printf("v = {%f, %f, %f}", v.x, v.y, v.z);

    return 0;
}
