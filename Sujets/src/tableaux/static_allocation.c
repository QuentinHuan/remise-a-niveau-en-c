#include <stdio.h>

int main(){

  int T[10] = {}; // initialisation d'un tableau à 10 élément:
  // allocation statique, donc la taille est fixée
  // à l'écriture du programme

  // affichage de chaques éléments
  for (int i = 0; i < 10; ++i) {

    printf("T[%d] = %d\n", i, T[i]);
  }

  return 0;
}
