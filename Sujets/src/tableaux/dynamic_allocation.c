#include <stdio.h>
#include <stdlib.h>

int main() {

  int n = 0;
  printf("taille du tableau ?");
  scanf("%d", &n); // entrée utilisateur sauvegardé dans (n)

  int *T = malloc(n * sizeof(int)); // allocation de mémoire (non initialisé)
  //   int *T = calloc(n, sizeof(int)); // allocation de mémoire (initialisé à 0)

  // affichage de chaque éléments
  for (int i = 0; i < n; ++i) {

    printf("T[%d] = %d\n", i, *(T + i)); // on peut aussi remplacer "*(T+i)" par "T[i]" pour alléger l'écriture
  }

  free(T); // libère l'espace mémoire alloué
  return 0;
}
