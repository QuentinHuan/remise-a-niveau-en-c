#include <stdio.h>

int sum(int T[], int n) {
  int r = 0;
  for (int i = 0; i < n; i++) {
    r += T[i];
  }
  return r;
}

int main() {
  int T[5] = {1, 2, 3, 4, 5};

  int *p = &T[0];

  for (int i = 0; i < 5; i++) {
    printf("&T[%i] = %p\n", i, (void *)&T[i]); // adresse de la case (i)
    printf("p = %p\n", (void *)p);             // adresse pointée
    printf("*p = %d\n", *p);                   // valeur pointée
    printf("-----------------\n");

    p++; // le pointeur bouge sur la case suivante
  }

  printf("%d", sum(T, 5));

  return 0;
}
