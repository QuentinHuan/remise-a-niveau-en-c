#include "exercices.h"

void hello(){
    printf("hello include\n");
}

int random_int(int max){
    return rand()%max;
}

void print_array(int T[], int size){
    for(int i=0; i<size;i++){
        printf("T[%d] = %d\n", i, T[i]);
    }
}

int max_array(int T[], int size){
    int max=T[0];
    for(int i=0; i<size;i++){
        if(max < T[i])
            max = T[i];
    }
    return max;
}

void test_pointer(){
    int a = 4;
    int* p;
    p = &a; // on fait pointer (p) sur l'adresse de (a)
            // quelques tests:
    printf("valeur de a: %d\n", a);
    printf("adresse mémoire de a: %p\n", (void *)&a); // affichage de l'adresse
    printf("\n");
    printf("valeur du pointeur p: %p\n", (void *)p); // affichage de l'adresse
    printf("valeur de a en passant par le pointeur p: %d\n", *p);
    *p = 0; // modification de a en utilisant le pointeur
    printf("valeur de a: %d\n", a);
}

void swap(int* a, int* b){
    int t=*a;
    *a = *b;
    *b = t;
}

void print_debug_array(int* p, int size){
    for (int i = 0; i < size; i++) {
        printf("p = %p\n", (void *)p); // adresse pointée
        printf("*p = %d\n", *p); // valeur pointée
        printf("-----------------\n");
        p++; // le pointeur bouge sur la case suivante
    }
}

void print_array_pointer(int* p, int size){
    for (int i = 0; i < size; i++) {
        printf("*p = %d\n", *p); // valeur pointée
        p++; // le pointeur bouge sur la case suivante
    }
}

void multiply_array(int* p, int size, int factor){
    for (int i = 0; i < size; i++) {
        *p = *p * factor;
        ++p;
        // ou bien:
        /* p[i] = p[i] * factor; */
    }
}

int* random_array(int size){
    int* T = malloc(size*sizeof(int));

    for(int i=0; i<size; ++i){
        T[i] = random_int(25);
    }
    return T;
}

int argmax_array(int* T, int size){
    int max=0;
    for(int i=0; i<size;i++){
        if(T[max] < T[i])
            max = i;
    }
    return max;
}

// tri à bulle
void sort_array(int* p, int size){
    for(int i=0; i<size; i++){
        int b=2;
        // size-i pour ne pas dépasser: argmax in [0,size-i]
        // On ajoute i pour compenser le décallage: argmax + i in [i, size_i]
        int argmax = argmax_array(&p[i], size-i) + i; 
        // debug:
        // printf("argmax_i = %d\n", argmax);
        // print_array_pointer(p, size);
        // printf("\n");
        //

        // on permute la case i avec la case maximale du reste
        swap(&p[i], &p[argmax]);
    }
}

int** random_array_2d(int size){
    int** T = malloc(size*sizeof(int*)); // tableau de (int*)

    for(int i=0; i<size; ++i){
        T[i] = malloc(size*sizeof(int)); // pour chaque (int*) on fait un tableau de (int)

        // initialisation de T[i] avec des nombres aliatoires
        for(int j=0; j<size; j++){
            T[i][j] = random_int(25);
        }
    }
    return T;
}

void print_array_2d(int** p, int size){

    for(int i=0; i<size; ++i){
        // initialisation de T[i] avec des nombres aliatoires
        for(int j=0; j<size; j++){
            printf("%d, ", p[i][j]);
        }
        printf("\n");
    }

}

void Pixel_print(struct Pixel* p){
        printf("{%d, %d, %d}\n", p->r, p->r, p->g);
        // on pourrait aussi faire:
        // print_array_pointer(p, 3); // car les champs de Pixel sont stockés les uns à la suite des autres
}

struct Pixel Pixel_add(struct Pixel* p1, struct Pixel* p2){
    struct Pixel p_out;
    p_out.r = p1->r + p2->r;
    p_out.g = p1->g + p2->g;
    p_out.b = p1->b + p2->b;

    return p_out;
}

struct Pixel Pixel_multiply(struct Pixel* p1, struct Pixel* p2){
    struct Pixel p_out;
    p_out.r = p1->r * p2->r;
    p_out.g = p1->g * p2->g;
    p_out.b = p1->b * p2->b;

    return p_out;
}


void print_image(struct Pixel** image, int size){

    for(int i=0; i<size; ++i){
        // initialisation de T[i] avec des nombres aliatoires
        for(int j=0; j<size; j++){
            struct Pixel p = image[i][j];
            printf("{%d, %d, %d}, ", p.r, p.r, p.g);
        }
        printf("\n");
    }

}

struct Pixel** random_image(int size){
    struct Pixel** image = malloc(size*sizeof(struct Pixel*)); // tableau de (int*)

    for(int i=0; i<size; ++i){
        image[i] = malloc(size*sizeof(struct Pixel)); // pour chaque (int*) on fait un tableau de (int)

        // initialisation de image[i] avec des nombres aliatoires
        for(int j=0; j<size; j++){
            struct Pixel p;
            p.r = random_int(255);
            p.g = random_int(255);
            p.b = random_int(255);
            image[i][j] = p;
        }
    }
    return image;
}

