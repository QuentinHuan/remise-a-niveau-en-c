#include "screen.h"


void hide_cursor(){
    printf("\x1b[?25l"); // hidden
}

void show_cursor(){
    printf("\x1b[?25h"); // shown
}

void move_cursor(int x, int y){
    printf("\033[%d;%dH", y, x);
}

void add_marker(char* marker, int position_x, int position_y){
        move_cursor(position_x, position_y);
        printf("%s", marker);
}



void draw_line(char* marker, int length, int direction_x,int direction_y, int origin_x, int origin_y){
    for (int i=0; i<length; i++){
        move_cursor(origin_x+i*direction_x, origin_y+i*direction_y);
        printf("%s",marker);
    }
}

void draw_border_unicode(int origin_x, int origin_y ,int width ,int height){

    draw_line("\u2501",width,1,0,origin_x+1,origin_y);
    draw_line("\u2501",width,1,0,origin_x+1,origin_y+height+1);
    draw_line("\u2503",height,0,1,origin_x,origin_y+1);
    draw_line("\u2503",height,0,1,origin_x+width+1,origin_y+1);

    add_marker("\u250F", origin_x, origin_y);
    add_marker("\u2513", origin_x+width+1, origin_y);

    add_marker("\u2517", origin_x, origin_y+height+1);
    add_marker("\u251B", origin_x+width+1, origin_y+height+1);
}
