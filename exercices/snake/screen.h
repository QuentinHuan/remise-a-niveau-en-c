#pragma once
#include <stdio.h>

void hide_cursor();
void show_cursor();
void move_cursor(int x, int y);
void add_marker(char* marker, int position_x, int position_y);

void draw_line(char* marker, int length, int direction_x,int direction_y, int origin_x, int origin_y);
void draw_border_unicode(int origin_x, int origin_y ,int width ,int height);
