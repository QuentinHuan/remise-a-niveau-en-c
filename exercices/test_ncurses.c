// gcc test_ncurses.c -o test_ncurses -lncurses && ./test_ncurses
#include <ncurses.h>
#include <time.h>

int main()
{
    /* setbuff(stdout, NULL); */
    initscr();			/* Start curses mode		  */
    printw("Hello World !!!");	/* Print Hello World		  */
    refresh();			/* Print it on to the real screen */
    timeout(1);

    while(1){
        /* printf("%d", (int)time(NULL)); */

        int c=     getch();			/* Wait for user input */
        /* printw ("                       \r"); */
        if(c!=-1){

            printw ("%d %c\r", c, c);
        }
        else{

            printw ("%d %c\r", 1, 1);
        }
        /* refresh(); */

    }
    endwin();			/* End curses mode		  */

    return 0;
}
