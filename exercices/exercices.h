#ifndef EXERCICES_H
#define EXERCICES_H
#include <stdio.h>

// I
void hello();

// II
int random_int(int max);
void print_array(int T[], int size);
int max_array(int T[], int size);

// III
void test_pointer();
void swap(int* a, int* b);
void print_debug_array(int* p, int size);
void print_array_pointer(int* p, int size);
void multiply_array(int* p, int size, int factor);

// IV
int* random_array(int size);
int argmax_array(int* p, int size);
void sort_array(int* p, int size);
int** random_array_2d(int size);
void print_array_2d(int** p, int size);

// V
struct Pixel{
    int r;
    int g;
    int b;

};

// IMG

void Pixel_print(struct Pixel* p);
void print_image(struct Pixel** image, int size);
struct Pixel Pixel_add(struct Pixel* p1, struct Pixel* p2);
struct Pixel Pixel_multiply(struct Pixel* p1, struct Pixel* p2);
struct Pixel** random_image(int size);



#endif

