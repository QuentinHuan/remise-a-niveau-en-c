#include <stdio.h>
#define STBIW_ASSERT(x)
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"
#include "image.h"

struct complex c_add(struct complex z, struct complex w){
    struct complex c = {z.re+w.re, z.im + w.im};
    return c;
}

struct complex c_mult(struct complex z, struct complex w){
    struct complex c;
    c.re = z.re*w.re - z.im*w.im;
    c.im = z.im*w.re + z.re*w.im;
    return c;
}

float c_module(struct complex z){
    return sqrt(z.re*z.re + z.im*z.im);
}

float z_n(struct complex c, int max_iter){
    struct complex z0 = {0.f, 0.f};
    struct complex zn = {0.f, 0.f};

    for(int i=0; i<max_iter; i++){
        zn = c_add( c_mult(zn,zn), c);
        z0 = zn;

        if(c_module(z0) > 2)
            return (float)i/(float)max_iter;
    }
    return 0.f;

}

void mandelbrot(){
    printf("MANDELBROT...");
    const int width = 2048;
    const int channel = 1;
    const int max_iter = 100;

    const float cam_x = 0.25;
    const float cam_y = 0.0f;
    const float cam__zoom = 2.5f;

    int8_t* pixels = malloc(width * width * channel * sizeof(int8_t));

    int8_t index = 0;
    for (int i = 0; i < width; i++){
        for(int j = 0; j < width; j++){
            float x = cam__zoom*((float)i/(float)width -0.5f - cam_x);
            float y = cam__zoom*((float)j/(float)width -0.5f - cam_y);
            struct complex c; c.re = x; c.im = y;
            int8_t p = z_n(c, max_iter) * 255;

            pixels[j*width +i] = p;
        }
    }

    stbi_write_jpg("fractal_.jpg", width, width, channel, pixels, 0);
    printf("MANDELBROT DONE");

}

void write_white_img(){

    const int width = 100;
    const int height = 100;
    const int channel = 3;

    int8_t* pixels = malloc(width * height * channel * sizeof(int8_t));

    int8_t index = 0;
    for (int i = 0; i < width*height*channel; i=i+3){
                int8_t r = 255;
                int8_t g = 255;
                int8_t b = 255;

                pixels[i+0] = r;
                pixels[i+1] = g;
                pixels[i+2] = b;
        }

    stbi_write_jpg("stbpng.jpg", width, height, channel, pixels, 0);

}
