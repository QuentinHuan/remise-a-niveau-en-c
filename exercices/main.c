#include <stdio.h>
#include "exercices.h"
#include "image.h"

// 1

void Exercices_I(){
    printf("\nExercices I:\n");
    printf("hello\n");
    hello();
}

// 2
// voir exercices.h
void Exercices_II(){
    printf("\nExercices II:\n");
    printf("\n1)\n");
    int size = 10;
    int T[10] = {0};
    for(int i=0; i<size; i++){
        int r = random_int(10);
        T[i] = r;
        printf("random_int: %d\n", r);
    }

    printf("\n2)\n");
    print_array(T, size);
    printf("\n3)\n");
    printf("maximum: %d\n", max_array(T,size));
}

void Exercices_III(){
    printf("\nExercices II:\n");
    printf("\n1)\n");
    test_pointer();

    printf("\n2)\n");
    int a = 1;
    int b = 2;
    printf("a=%d, b=%d\n", a,b);
    printf("swap(&a, &b);\n");
    swap(&a, &b);
    printf("a=%d, b=%d\n", a,b);

    printf("\n3)\n");
    int T[5] = {0,1,2,3,4};
    int* p = &T[0];
    print_debug_array(p, 5);
    printf("\nmultiply array by 10;\n\n");
    multiply_array(p, 5, 10);
    print_debug_array(p, 5);
}

void Exercices_IV(){
    printf("\nExercices IV:\n");
    printf("\n1)\n");
    int size = 5;
    int* T = random_array(size);
    print_array_pointer(T, size);

    printf("\n2)\n");
    sort_array(T, size);
    print_array_pointer(T, size);

    printf("\n3)\n");
    int** T_2d = random_array_2d(size);
    print_array_2d(T_2d, size);

    // libérer T
    free(T);

    // libérer T_2d
    for(int i=0; i<size; i++){
        free(T_2d[i]); // on libère les (int*)
    }
    free(T_2d); // on libère le ( int** )
}

void Exercices_V(){
    printf("\nExercices V:\n");
    printf("\n1)\n");
    printf("pixel_1: \n");

    struct Pixel pixel_1;
    pixel_1.r=10;
    pixel_1.g=255;
    pixel_1.b=100;

    Pixel_print(&pixel_1);

    printf("\n2)\n");
    struct Pixel pixel_2;
    pixel_2.r=50;
    pixel_2.g=23;
    pixel_2.b=0;
    printf("pixel_2: \n");
    Pixel_print(&pixel_2);

    struct Pixel pixel_add = Pixel_add(&pixel_1, &pixel_2);
    struct Pixel pixel_mult = Pixel_multiply(&pixel_1, &pixel_2);

    printf("pixel_1 + pixel_2: \n");
    Pixel_print(&pixel_add);

    printf("pixel_1 * pixel_2: \n");
    Pixel_print(&pixel_mult);

    printf("\n3)\n");
    int size_image = 5;
    struct Pixel** image = random_image(size_image);
    print_image(image, size_image);

    // libérer image
    for(int i=0; i<size_image; i++){
        free(image[i]); // on libère les (int*)
    }
    free(image); // on libère le ( int** )

}

void Exercices_IMG(){
    write_white_img();
    mandelbrot();
}


int main(int argc, char *argv[]){
    Exercices_I();
    Exercices_II();
    Exercices_III();
    Exercices_IV();
    Exercices_V();
    Exercices_IMG();

    return 0;
}
