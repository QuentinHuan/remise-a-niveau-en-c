#ifndef IMAGE_H
#define IMAGE_H
#include <stdio.h>
struct complex{
    float re;
    float im;
};
struct complex c_add(struct complex z, struct complex w);
struct complex c_mult(struct complex z, struct complex w);
float c_module(struct complex z);
float z_n(struct complex c, int max_iter);
void mandelbrot();
void write_white_img();




#endif
